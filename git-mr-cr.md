# Git Overview: Branching, Merge Requests, and Code Review

## Git Overview and Basics

- Git is a distributed version control system designed to handle everything from small to large projects with speed and efficiency.
- Version control is a system that records changes to a file or set of files over time so that you can recall specific versions later.
- In a distributed version control system  clients don’t just check out the latest snapshot of the files; rather, they fully mirror the repository, including its full history. Operations in Git need only local files and resources to operate.

### Git Repositories

- Files are stored in a repository. A repository is similar to how you store files in any folder or directory on your computer.
- A remote repository refers to the files in GitLab.
- A local copy refers to the files on your computer.
- Changes are first made locally then *pushed* to a remote repository.
- Changes from the remote copy are made available locally by *pulling* from the remote repository.

### Clone

- To create a copy of a remote repository’s files on your computer, you *clone* it.You can then modify the files locally and upload the changes to the remote repository on GitLab.



### Pull and Push (Keeping updated/Making Changes available to others)

- When the remote repository changes, your local copy is behind. You can update your local copy with the new changes in the remote repository. This is referred to as pulling from the remote, because you use the command `git pull`.
- After you save a local copy of a repository and modify the files on your computer, you can upload the changes to GitLab. This is referred to as pushing to the remote, because you use the command `git push`.


### Demo 1 (Cloning, Pulling, viewing history)

- https://git.systems.wvu.edu/jsmith39/cvs-demo-2021-08-10

#### Cloning - Getting an initial copy of a repository.

 - `git clone https://git.systems.wvu.edu/jsmith39/cvs-demo-2021-08-10.git`

#### Pulling - Staying up to date with a remote repository

- `cd $PROJECT_DIR; git pull`

#### Viewing History

- `git log`

### Making Changes.

- Before making changes it's a good idea to ensure your local copy contains the latest changes from the remote repository: `git pull`.
- Edit the files you wish to change and save those changes as you normally would.
- Make git aware of those changes using the command `git commit $FILENAME(S)`
- Upload those changes to the remote repository using the command `git push`.

## Branches

- A branch is a copy of the files in the repository at the time you create the branch.
- You can work in your branch without affecting other branches.
- When you’re ready to add your changes to the main codebase, you can merge your branch into the default branch, for example, master.
- A new branch is often called feature branch to differentiate from the default or master branch.
- It is possible to make changes directly to the master branch, however when collaborating with others.it is considered a best practice to make all changes to a feature branch before incorporating them into the default branch.

### Demo 2 (Branching)

#### Creating a Branch and Making Changes within the branch.

- Branch names cannot contain empty spaces and special characters. Use only lowercase letters, numbers, hyphens, and underscores.

A feature branch is created using the

`git checkout -b $BRANCH_NAME`

command.

```
git checkout -b fix_typeo
git commit git-mr-cr.md
git push
```

- What happened?

`git push $REMOTE $NAME_OR_BRANCH`

- Typically $REMOTE will be `origin`.

`git push --set-upstream origin fix_typeo`


## Merge Requests

- In GitLab, you typically use a merge request to merge your changes into the default branch.
- Merge requests (MRs) are the way you check source code changes into a branch. When you open a merge request, you can visualize and collaborate on the code changes before merging.

### Typical Merge Request Workflow

1. You checkout a new branch, and submit your changes for inclusion in the default branch through a merge request.
1. You gather feedback from your team (Code Review).
1. Wait on 'approval'
1. If approved - merge to default branch

### Demo 3 (Creating an MR)

## Code Review

- Code (or Peer) Review is a collaborative process in which one or several people check or review the changes to a codebase before those changes are merged into the default branch.
- At least one of the reviewers should not be the code's author.
- Code Reviews are performed with a variety of goals in mind, including:
	- Improve code quality.
	- Learning/Knowledge Transfer
	- Increase sense of mutual responsibility
- Code review isn't intended to be punitive but instead should be considered an opportunity for us all to learn together.

### Demo 4 (Approving an MR)

### Demo 5 (Merging a branch with an approved Merge Request)

- Merging a feature branch into the default branch is typically the responsibility of the branch's author not the reviewers/approvers.O
